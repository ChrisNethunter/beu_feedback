# beu_feedback_app

Este es un proyecto base hecho desde cero con estructura basada modelo, vista, controlador.
Como todo còdigo siempre se puede mejorar, espero les agrade.
Esto lo que pude lograr en el tiempo pactado, siempre estoy abierto a opiniones de mejora.

El proyecto al iniciar ejecuta las migraciones con el archivo data.json, este inserta un usuario por defecto con contraseña y sus respectivos datos.

La base de datos esta en mongodb llamada beufeedback
usuario por defecto para la plataforma

Tareas pendientes:
- zona de comentarios
- filtros de vista suggestion

Construir esta aplicación de recolección de feedback para productos y que cumpla con los requerimientos visuales y funcionales.


# INICIAR PROYECTO
    npm i 
    npm run dev
# REQUISITOS 

    ## mongodb
    ## nodejs v14.17.5 o superior
    ## pm2 global para pruebas de despliegue
    
# FRONTEND
El frontend esta estructurado de la siguiente manera en la carpeta src :

    ## layouts Contenedores para el control vistas base de cada seccion de la app.
    ## pages Secciones que componen la app.
    ## helpers Clases dinamicas orientadas a ser reutilizadas de acuerdo a su necesidad.
    ## styles Archivos de hojas de estilo de la app.

# BACKEND
El frontend esta estructurado de la siguiente en la carpeta project :

    ## dao ( data access model ) Aquì se contiene la lògica de acceso de datos por cada modelo.
    ## models Se contiene la estructura de datos por cada tabla ya sea sql o en este caso colecciones.
    ## helpers Clases dinamicas orientadas a ser reutilizadas de acuerdo a su necesidad.
    ## routes Aqui se contiene toda la api tano rutas publicas como privadas.
    ## middleware Validadores de seguridad.

# DEPLOY
    Este proyecto esta configurado para ser desplegado con Pm2, se puede mejorar con docker y algunas configuraciones adicionales de seguridad como llaves pem y una configuraciòn de despliegue remoto.

# Proyecto React + Express 

Este proyecto base fue creado con [Create React App](https://github.com/facebook/create-react-app).

## Scripts

Comandos para iniciar el proyecto:

### `npm dev`

Inicia el proyecto en modo desarrollo.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm prod`

Inicia el proyecto en modo produccion.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Construye la aplicación para producción en la carpeta `build`. \
Agrupa correctamente React en el modo de producción y optimiza la compilación para obtener el mejor rendimiento.

La compilación se minimiza y los nombres de archivo incluyen los hash. \

[deployment](https://facebook.github.io/create-react-app/docs/deployment) 

### `npm run eject`

**Note: Esta es una operación unidireccional. Una vez usa el comando `eject`, no podra volver a atras!**

Si no está satisfecho con la herramienta de compilación y las opciones de configuración, puede "expulsar" en cualquier momento. Este comando eliminará la dependencia de compilación única de su proyecto.

En su lugar, copiará todos los archivos de configuración y las dependencias transitivas (paquete web, Babel, ESLint, etc.) directamente en su proyecto para que tenga un control total sobre ellos. Todos los comandos excepto `eject` seguirán funcionando, pero apuntarán a los scripts copiados para que pueda modificarlos. En este punto estás solo.

No es necesario que utilice nunca "eject". El conjunto de funciones seleccionadas es adecuado para implementaciones pequeñas y medianas, y no debe sentirse obligado a utilizar esta función. Sin embargo, entendemos que esta herramienta no sería útil si no pudiera persona

### `npm prod`

Inicia el proyecto en modo produccion.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm run deploy`

Este comando inicia el proyecto en modo produccion con pm2 con su ambiente de produccion ecosystem.config.js
[https://pm2.keymetrics.io] 

### `npm run restart_deploy`

Este comando reinicia el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run stop_deploy`

Este comando detiene el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run delete_deploy`

Este comando detiene el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run monitor_deploy`

Este comando abre el monitor de recursos del servidor con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run status_deploy`

Este comando nos reporta el estado del servidor con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run logs_deploy`

Este comando nos muestra los logs  del servidor en tiempo real con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run docs`

Este comando genera la documentacion del proyecto con jsdoc
[https://jsdoc.app] 
