import React from 'react';
import ReactDOM from 'react-dom';

import './styles/styles.css';
import './styles/hover.css';
import './styles/grid.css';
import './styles/spinkit.css';
import './styles/responsive.css';

import App from './App';

ReactDOM.render(<App />,document.getElementById('root'));