
import React, { Component } from 'react';

import Icon from '../components/Icon';
import Button from '../components/Button';
import DropDown from '../components/DropDown';
import FeedBackItem from '../components/FeedBackItem';
import TextSort from '../components/TextSort';
import NoDataFeedBack from '../components/NoDataFeedBack';
import Sidebar from '../components/Sidebar';
import NavbarMobile from '../components/NavbarMobile';

import LocalStorage from '../helpers/localstorage';
import Auth from '../controllers/auth';
import SuggestionsController from '../controllers/suggestions';
import CategoriesController from '../controllers/categories';

class Suggestions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			list_suggestions : [],
			user:{},
			categories:[],
			roadMapCount:{
				planned : 0,
				live:0,
				inProgress:0,
				suggestions:0
			},
			activefilter: "all"
		}
    }

	componentDidMount(){
		this.getDataSuggestions({ status : "suggestion" });
		this.getUser();
		this.getAllCategories();
		this.getDataRoadMapCount();
	}
	
	handlerClickCreateFeedback = (event) => {
		event.preventDefault();
		window.location = "/newfeedback";
	}

	handlerClickLogOut = (event) => {
		event.preventDefault();
		LocalStorage.clearLocalStorage();
		window.location.href="/";
	}

	handlerClickFilterCategory = (event) => {
		event.preventDefault();
		let id = event.currentTarget.id;
		let query = {};
		if(id == "all"){
			this.setState({activefilter: 'all'})
			query = {};
		}else{
			this.setState({activefilter: id})
			query = { category : id , status : "suggestion" };
		}
		this.getDataSuggestions(query);
	}
	
	getDataSuggestions = async (filter) =>{
		let query  = {
			filter : filter
		}
		let getData = await SuggestionsController.getSuggestions(query);
		if(getData.success){
			for (let item in getData.data){
				let category_name = await this.getCategoryName(getData.data[item].category);
				getData.data[item].category = category_name;
			}
			this.setState({
				list_suggestions: getData.data
			});
		}
	}

	getCategoryName(category_id) {
		return new Promise( async (resolve, reject) => {
			let category = await CategoriesController.getCategories(category_id)
			return resolve(category.data.name)
		})
	}

	async getDataRoadMapCount(){
		let getData = await SuggestionsController.getDataRoadMap();
		if(getData.success){
			this.setState({roadMapCount : getData.data})
		}
	}

	async getAllCategories () {
		let dataCategory = await CategoriesController.getAllCategories();
		this.setState({
			categories : dataCategory.data
		});
    }

	getUser(){
        Auth.getUser( ( response ) => {
			if(response.success || Object.keys(response.data).length ){
                this.setState({ user : response.data })
            }
        });
    }

	render() {
		return (
            <div className="row content-layout-feedbacks">
				
				{/*leftaside*/}
				<Sidebar 
					user={this.state.user}
					categories={this.state.categories}
					roadMapCount={this.state.roadMapCount}
					activefilter={this.state.activefilter}
					handlerClickFilterCategory = {this.handlerClickFilterCategory}
					handlerClickLogOut={this.handlerClickLogOut}
				/>
				{/*leftaside*/}
				
				{/*navmobile*/}
				<NavbarMobile
					user={this.state.user}
				/>
				{/*navmobile*/}	

				<div className="col-md-12 col-lg-9 col-sm-12">

					{/*navbar*/}
					<nav className="content-feedbacks" >
						<div className="navbar navbar-expand-lg navbar-dark-ocean bg-dark-ocean">
							<div className="row">
								<div className="col-lg-2 col-md-2 col-xs-1 nav-mobile">
									<div className="">
										<h3 className="navbar-brand spacing-left" >
											<Icon 
												type="IconSuggestion"
												className=" icon-btn-white"/>
										</h3>
									</div>
								</div>
								<div className="col-lg-4 col-md-5 col-xs-1  nav-mobile">
									<h3 className="navbar-brand text-start" >
										{this.state.roadMapCount.suggestions} Suggestions
									</h3> 
								</div>
								<div className="col col-lg-4 col-md-3 col-sm-12">
									<DropDown 
										textButton={ TextSort }
										shadow={false}
										>
										<a className="dropdown-item" href="#">Most Upvotes</a>
										<div className="dropdown-divider"></div>
										<a className="dropdown-item" href="#">Lest Upvotes</a>
										<div className="dropdown-divider"></div>
										<a className="dropdown-item" href="#">Most Comments</a>
										<div className="dropdown-divider"></div>
										<a className="dropdown-item" href="#">Least Comments</a>
										<div className="dropdown-divider"></div>
									</DropDown>
								</div>
								<div className="col col-lg-2 col-md-1 col-sm-12">
									<Button
										text={(
											<div>
												<Icon 
													type="IconPlus"
													className="icon-btn icon-btn-white icon-add-feedback" />
													Add Feedback
											</div>
										)}
										custom={{ 
											"className":`btn btn-one icon-btn-back btn-add-feedback`,
											onClick:this.handlerClickCreateFeedback
										}}/>
								</div>
							</div>
						</div>
					</nav>
					{/*navbar*/}

					{/*listfeedbacks*/}
					<div className="container-feedbacks">
						{
							this.state.list_suggestions.length ?
								this.state.list_suggestions.map( ( suggestion , key ) =>
									<FeedBackItem 
										key={key}
										suggestion={suggestion} 
										detail={() => this.handlerClickDetailFeedback(suggestion._id)}/>
								)
							:
							<NoDataFeedBack/>
						}
					</div>
					{/*listfeedbacks*/}
			   	</div>   
            </div>
		);
	}
}

export default Suggestions;