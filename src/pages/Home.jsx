
import React, { Component } from 'react';

import Button from '../components/Button';
import Icon from '../components/Icon';
import Select from '../components/Select';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
            user : {}
        }
    }

	render() {
		return (
            <div className="App row">
                <div className="col-md-6">
                    <h1 className="text-ocean"> Test 1</h1>
                    <h2 className="text-ocean"> Test 2</h2>
                    <h3 className="text-ocean"> Test 3</h3>
                    <h4 className="text-ocean"> Test 4</h4>
                </div>

                <div className="col-md-5">
                    <p className="text-ocean">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    </p>

                    <p className="body-content-two text-ocean">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    </p>
                    
                    <p className="body-content-three text-ocean">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                    </p>
                </div>

                <div className="col-md-12">
                <Button 
                    className="btn btn-one icon-btn-back" 
                    >
                    Button 1
                </Button>
                <br />
                <br />
                <button className="btn btn-two">Button 2</button>
                <br />
                <br />
                <button className="btn btn-three">Button 3</button>
                <br />
                <br />
                <button className="btn btn-four">Button 4</button>
                <br />
                <br />
                <Button 
                    className="btn btn-five" 
                    >
                    <Icon 
                        type="IconArrowLeft"
                        className="icon-btn icon-btn-blue" />
                    Button 5
                </Button>
                <br />
                <br />
                <Button 
                    className="btn btn-six" 
                    >
                    <Icon 
                        type="IconArrowLeft"
                        className="icon-btn icon-btn-white" />
                    Button 6
                </Button>
                <br />
                <br />
               {/*  <DropDown>
                    <a className="dropdown-item" href="#">Action</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">Another action</a>
                    <div className="dropdown-divider"></div>
                    <a className="dropdown-item" href="#">Something else here</a>
                </DropDown>

                <br />
                <br /> */}
                </div>
                <form style={{width : "30%"}}>
                    <label className="form-label">Test 1</label>
                    <input 
                        type="email" 
                        className="form-control" 
                        id="exampleInputEmail1" />
                    <div 
                        id="emailHelp" 
                        className="form-text ">
                        We'll never share your email with anyone else.
                    </div>

                    <label className="form-label">Test 2</label>
                    <input 
                        type="email" 
                        className="form-control is-invalid" 
                        id="exampleInputEmail1" />
                    <div 
                        id="emailHelp" 
                        className="form-text invalid-feedback">
                        We'll never share your email with anyone else.
                    </div>

                    
                    <label className="form-label">Example textarea</label>
                    <textarea className="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    
                    
                    {/* <div className="custom-select" style={{width: "20%"}}>
                        <label class="form-label">Example select</label>
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Open this select menu</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div> */}
                </form>

                <br />
                <br />

             
                <Select>
                    <option value={0}>Select</option>
                    <option value={1}>Audi</option>
                    <option value={2}>BMW</option>
                    <option value={3}>Citroen</option>
                    <option value={4}>Ford</option>
                    <option value={5}>Honda</option>
                    <option value={6}>Jaguar</option>
                    <option value={7}>Land Rover</option>
                    <option value={8}>Mercedes</option>
                    <option value={9}>Mini</option>
                    <option value={10}>Nissan</option>
                    <option value={11}>Toyota</option>
                    <option value={12}>Volvo</option>
                </Select>
               
            </div>
		);
	}
}

export default Home;
