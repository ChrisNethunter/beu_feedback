
import React, { Component } from 'react';

import Validations  from '../helpers/validations';
import Button from '../components/Button';

import Auth from '../controllers/auth';

class Register extends Component {
	constructor(props) {
		super(props);
		this.state = {}
    }
	
	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/";
	}

	handlerSubmitRegister = (event) => {
		event.preventDefault();
		let form = event.currentTarget;
		let validateForm = Validations.validateForm(form.elements);
		if(!validateForm.status) return false 

		if( this.refs.password.value != this.refs.passwordConfirm.value ){
			alert("Passwords no match")
			return false
		}

		let data = {
			email : this.refs.email.value,
			name : this.refs.name.value,
			password : this.refs.password.value
		}

		Auth.register( data ,( response ) => {
			form.reset()
			if (response.success){
				window.location.href = '/login'
			}else{
				alert(response.message)
			}
        });
	}

	handlerClickLogin = (event) => {
		event.preventDefault();
		window.location.href = '/login'
	}

	render() {
		return (
			<div className="row ">
				<div className="col-12">
					<div className="content-feedback-form">
						<div className="card" style={{ maxWidth : "45rem" , padding : "3%"}}>
							<div className="card-body content-post-comment">
								<h2 className="text-ocean" >Register </h2>
								<br />
								
								<form onSubmit={this.handlerSubmitRegister}>
									<div className="form-element">
										<label className="form-label">Email</label>
										<input
											id="email" 
											type="email" 
											className="form-control" 
											ref="email" />
										<div id="email-validate" className="invalid-feedback">
											Email invalid
										</div>
									</div>
									<br />
									<div className="form-element">
										<label className="form-label">Full name</label>
										<input
											id="name"  
											type="text" 
											className="form-control" 
											ref="name" />
										<div id="name-validate" className="invalid-feedback">
											Name invalid
										</div>
									</div>
									<br />
									<div className="form-element">
										<label className="form-label">Password</label>
										<input 
											id="password"
											type="password" 
											className="form-control" 
											ref="password" />
										<div id="password-validate" className="invalid-feedback">
											Password invalid
										</div>
									</div>
									<br />
									<div className="form-element">
										<label className="form-label">Confirm password</label>
										<input
											id="password-confirm" 
											type="password" 
											className="form-control" 
											ref="passwordConfirm" />
										<div id="password-confirm-validate" className="invalid-feedback">
											Password invalid
										</div>
									</div>
								
									<br />
									<br />
									<div className="form-footer">
										<Button type="submit" className="btn btn-one btn-block" >
											Create account
										</Button>
									</div>
								</form>

								<div align="center" style={{marginTop:20}}>
									<a 	
										onClick={this.handlerClickLogin}
										className="btn btn-five" >
										Login
									</a> 
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Register;