
import React, { Component } from 'react';
import { withRouter } from "react-router";
import swal from 'sweetalert2'

import Icon from '../components/Icon';
import Button from '../components/Button';
import Select from '../components/Select';

import CategoriesController from '../controllers/categories';
import SuggestionsController from '../controllers/suggestions';
import Validations from '../helpers/validations';
import Alerts from '../helpers/alerts';

class EditFeedback extends Component {
	constructor(props) {
		super(props);
		this.state = {
			categories: [],
			suggestion: {}
		}
		this.status_suggestions = [
			{ name: "Select", id: "0" },
			{ name: "Suggestion", id: "suggestion" },
			{ name: "Planned", id: "planned" },
			{ name: "In-Progress", id: "in-progress" },
			{ name: "Live", id: "live" },
		]
	}

	componentDidMount() {
		this.getSuggestionData();
		this.getAllCategories();
	}

	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/";
	}

	handlerDelete = (event) => {
		event.preventDefault()
		let self = this
		swal({
			title: 'Are you sure?',
			text: 'This accion have not been restored!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, keep it'
		}).then(async function () {
			SuggestionsController.deleteSuggestion({_id: self.props.match.params._id}, (err, response) => {
				swal(
					'Deleted!',
					'Suggestion has been deleted!',
					'success'
				)
				window.location.href = '/'
			})
		}, function (dismiss) {
			if (dismiss === 'cancel') {
				swal(
					'Cancelled',
					'Accion has cancelled!',
					'error'
				)
			}
		})

	}

	getSuggestionData = async () => {
		let data = await SuggestionsController.getSuggestions({ filter: { _id: this.props.match.params._id } })
		if (data.success) {
			let current = data.data[0]
			this.setState({
				suggestion: current
			})
		}
	}

	async getAllCategories() {
		let dataCategory = await CategoriesController.getAllCategories();
		this.setState({
			categories: dataCategory.data
		});
	}

	handlerSubmitEditFeedback = (event) => {
		event.preventDefault();
		let form = event.currentTarget;
		let validateForm = Validations.validateForm(form.elements);
		if (!validateForm.status) return false

		let suggestion = {
			_id: this.props.match.params._id,
			title: this.refs.title.value,
			category: document.getElementById("category").value,
			status: document.getElementById("status-feedback").value,
			description: this.refs['description-validate'].value,
		}

		SuggestionsController.editSuggestion(suggestion, (response) => {
			if (response.success) {
				Alerts.success("Feedback has edited");
				form.reset()
				setTimeout(() => {
					window.location = "/suggestions";
				}, 2000)
			}
		});
	}

	render() {
		return (
			<div className="row ">
				{Alerts.containerAlert()}
				<div className="col-12">
					<div className="content-back">
						<Button
							text={(
								<div>
									<Icon
										type="IconArrowLeft"
										className="icon-btn icon-btn-blue" />
									Go Back
								</div>
							)}
							custom={{
								"className": `btn btn-five`,
								onClick: this.handlerClickBack
							}} />
					</div>

					<div className="content-feedback-form">
						<div className="card" style={{ maxWidth: "45rem", padding: "3%" }}>
							<div className="card-body content-post-comment">
								<div style={{ marginTop: "-6rem" }}>
									<Icon type="IconEditFeedback" />
								</div>
								<h2 className="text-ocean" >Editing Add a dark theme option</h2>
								<br />
								<form onSubmit={this.handlerSubmitEditFeedback}>
									<div className="form-element">
										<label className="form-label">Feedback Title</label>
										<div
											id="title"
											className="form-text">
											Add a short, descriptive headline.
										</div>
										<input
											type="text"
											className="form-control"
											defaultValue={`${this.state.suggestion.title ? this.state.suggestion.title : ''}`}
											id="title"
											ref="title"
										/>
									</div>

									<div className="form-element">
										<label className="form-label">Category</label>
										<div
											className="form-text">
											Choose a category for yout feedback.
										</div>
										{this.state.categories.length > 0 ?
											<Select id="category" ref="category">
												{
													this.state.categories.map((category, key) =>
														<option key={key} value={category._id} selected={this.state.suggestion.category == category._id}>{category.name}</option>
													)
												}
											</Select>
											: ''}
									</div>

									<div className="form-element">
										<label className="form-label">Update Status</label>
										<div
											className="form-text">
											Change feedback state.
										</div>
										{this.status_suggestions.length > 0 ?
											<Select id="status-feedback" ref="status-feedback">
												{this.status_suggestions.map((status, key) =>
													<option key={key} value={status.id} selected={this.state.suggestion.status == status.id} >{status.name}</option>
												)}
											</Select>
											: ''}
									</div>

									<div className="form-element">
										<label className="form-label">Feedback Detail</label>
										<div
											id="description-validate"
											className="form-text">
											Include any specific comments on what should be improved, added, etc.
										</div>
										<textarea className="form-control" ref="description-validate" id="description-validate" rows="3"
											defaultValue={`${this.state.suggestion.description ? this.state.suggestion.description : ''}`}>
										</textarea>
									</div>

									<div className="form-footer">
										<Button
											text="Delete"
											custom={{
												"className": `btn btn-four`,
												onClick: this.handlerDelete
											}} />

										<Button
											text="Cancel"
											custom={{
												"className": `btn btn-three`,
												onClick: this.handlerClickBack
											}} />

										<Button
											text="Edit Feedback"
											custom={{
												"className": `btn btn-one`,
												type: "submit"
											}} />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(EditFeedback);