
import React, { Component } from 'react';

import Icon from '../components/Icon';
import Button from '../components/Button';
import CardRoadMap from '../components/CardRoadMap';
import SuggestionsController from '../controllers/suggestions';
import CategoriesController from '../controllers/categories';
class RoadMap extends Component {
	constructor(props) {
		super(props);
		this.state = {
			roadMapCount:{
				planned : 0,
				live:0,
				inProgress:0,
				suggestions:0
			},
			data: {},
			widthScreen:0,
			tab: "planned"
		}
    }

	componentDidMount(){
		this.getDataRoadMapCount();
		this.setState({
			widthScreen : window.screen.width
		});
		let context = this;
		window.addEventListener("resize", (event) => {
            context.setState({
				widthScreen : window.screen.width
			});
        })
	}

	async getDataRoadMapCount(){
		let getData = await SuggestionsController.getDataRoadMap();
		if(getData.success){
			this.setState({roadMapCount : getData.data, data: getData.data.data})
		}
	}

	handlerClickDetailFeedback = (id) => {
		window.location = `/feedbackdetail/${id}`;
	}
    
	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/suggestions";
	}

	handlerClickTab = (event) => {
		event.preventDefault();
		/* let items = document.getElementsByClassName("tab-item");
		for( let item of items){
			item.classList.remove("active")
		}
		event.currentTarget.classList.add("active") */
		this.setState({ tab : event.currentTarget.getAttribute("tab") });
		
		event.currentTarget.style.borderBottom = `solid 4px #${event.currentTarget.getAttribute("color")} !important;`

	}
	
	contentTab = () => {
		if(this.state.tab == "planned"){
			return (
				<div className="tab-pane" id="tab_default_1">
					<h2> Planned ({this.state.roadMapCount.planned})</h2>
					<p className="body-content-two spacing-top-roadmap">Ideas prioritized for research.</p>
					<CardRoadMap
						data={this.state.data}
						keyData='planned'
						text='Planned'
						classCustom='card-feedback-planned'
						color="purple"
					/>
				</div>
			)
		}else if(this.state.tab == "inprogress"){
			return(
				<div className="tab-pane" id="tab_default_2">
					<h2> In-progress ({this.state.roadMapCount.inProgress})</h2>
					<p className="body-content-two spacing-top-roadmap">Currently being develop.</p>
					<CardRoadMap
						data={this.state.data}
						keyData='inProgress'
						text='In-progress'
						classCustom='card-feedback-in-progress'
						color="salmon"
					/>
				</div>
			)
		}else{
			return(
				<div className="tab-pane" id="tab_default_3">
					<h2> Live ({this.state.roadMapCount.live})</h2>
					<p className="body-content-two spacing-top-roadmap">Released fetures.</p>
					<CardRoadMap
						data={this.state.data}
						keyData='live'
						text='Live'
						classCustom='card-feedback-live'
						color="bluelight"
					/>
				</div>
			)
		}
	}

	render() {
		return (
            <div className="container container-road-map">
				
				{/*navbar*/}
				<div className="">
					<nav className="navbar navbar-expand-lg navbar-dark-ocean bg-dark-ocean">
						<div className="container-fluid">
							<h3 className="navbar-brand roadmap-title" >
								<Button
									text={(
										<div>
											<Icon 
												type="IconArrowLeft"
												className="icon-btn icon-btn-white" />
											Go Back
										</div>
									)}
									custom={{ 
										"className":`btn btn-six roadmap-title-back`,
										onClick:this.handlerClickBack
									}}/>
								<br />
								Roadmap
							</h3> 
							<Button
								text={(
									<div>
										<Icon 
											type="IconPlus"
											className="icon-btn icon-btn-white icon-add-feedback" />
										Add Feedback
									</div>
								)}
								custom={{ 
									"className":`btn btn-one icon-btn-back btn-add-feedback`,
									onClick:this.handlerClickCreateFeedback
								}}/>
						</div>
					</nav>
				</div>
				{/*navbar*/}

				{
                    this.state.widthScreen <= 576 ?
						<div className="tabbable-panel">
							<div className="tabbable-line">
								<ul className="nav nav-tabs">
									<li className=" tab-item" tab="planned" color="#AD1FEA" onClick={this.handlerClickTab}>
										Planned 
									</li>
									<li className="tab-item" tab="inprogress" color="#F49F85" onClick={this.handlerClickTab}>
										In-progress 
									</li>
									<li className="tab-item" tab="live" color="#62BCFA" onClick={this.handlerClickTab}>
										Live
									</li>
								</ul>
								<div className="tab-content">
									{this.contentTab()}
								</div>
							</div>
						</div>
					:
						<div>
							<div className="row content-cards-feedbacks">
								<div className="col-4">
									<h2> Planned ({this.state.roadMapCount.planned})</h2>
									<p className="body-content-two spacing-top-roadmap">Ideas prioritized for research.</p>
								</div>
								<div className="col-4">
									<h2> In-progress ({this.state.roadMapCount.inProgress})</h2>
									<p className="body-content-two spacing-top-roadmap">Currently being develop.</p>
								</div>
								<div className="col-4">
									<h2> Live ({this.state.roadMapCount.live})</h2>
									<p className="body-content-two spacing-top-roadmap">Released fetures.</p>
								</div>
							</div>

							<div className="row content-cards-feedbacks">
								<div className="col-md-4 col-lg-4 col-sm-12">
									<CardRoadMap
										data={this.state.data}
										keyData='planned'
										text='Planned'
										classCustom='card-feedback-planned'
										color="purple"
									/>
								</div>

								<div className="col-md-4 col-lg-4 col-sm-12">
									<CardRoadMap
										data={this.state.data}
										keyData='inProgress'
										text='In-progress'
										classCustom='card-feedback-in-progress'
										color="salmon"
									/>
								</div>

								<div className="col-md-4 col-lg-4 col-sm-12">
									<CardRoadMap
										data={this.state.data}
										keyData='live'
										text='Live'
										classCustom='card-feedback-live'
										color="bluelight"
									/>
								</div>

							</div>
						</div>
				}
				

            </div>
		);
	}
}

export default RoadMap;