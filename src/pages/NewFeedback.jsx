
import React, { Component } from 'react';

import Icon from '../components/Icon';
import Button from '../components/Button';
import Select from '../components/Select';

import CategoriesController from '../controllers/categories';
import SuggestionsController from '../controllers/suggestions';
import Validations  from '../helpers/validations';
import Alerts from '../helpers/alerts';
import Messages from '../helpers/messages.json';

class NewFeedback extends Component {
	constructor(props) {
		super(props);
		this.state = {
			categories:[]
		}
    }
	
	componentDidMount(){
		this.getAllCategories();
	}

	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/suggestions";
	}

	handlerSubmitCreateFeedback = (event) => {
		event.preventDefault();
		let form = event.currentTarget;
		let validateForm = Validations.validateForm(form.elements);
		if(!validateForm.status) return false 
		
		let suggestion = {
			title : this.refs.title.value,
			category : document.getElementById("category").value,
			description : this.refs.description.value,
		}

		SuggestionsController.createSuggestion( suggestion ,( response ) => {
			if (response.success){
				Alerts.success("Feedback has created");
				form.reset()
				setTimeout( () => {
					window.location = "/suggestions";
				},1000)
			}else{
				Alerts.error(`${Messages["error"]} ${response.message}`);
			}
        });
	}
	
	async getAllCategories () {
		let dataCategory = await CategoriesController.getAllCategories();
		this.setState({
			categories : dataCategory.data
		});
    }

	render() {
		return (
			<div className="row ">
				{Alerts.containerAlert()}
				<div className="col-12">
					<div className="content-back">
						<Button
							text={(
								<div>
									<Icon 
										type="IconArrowLeft"
										className="icon-btn icon-btn-blue" />
									Go Back
								</div>
							)}
							custom={{ 
								"className":`btn btn-five`,
								onClick:this.handlerClickBack
							}}/>
					</div>

					<div className="content-feedback-form">
						<div className="card" style={{ maxWidth : "45rem" , padding : "3%"}}>
							<div className="card-body content-post-comment">
								<div style={{ marginTop : "-6rem"}}>
									<Icon type="IconNewFeedback"/>
								</div>
								<h2 className="text-ocean" >Create New Feeback </h2>
								<br />
								<form onSubmit={this.handlerSubmitCreateFeedback}>
									<div className="form-element">
										<label className="form-label">Feedback Title</label>
										<div  
											className="form-text">
											Add a short, descriptive headline.
										</div>
										<input
											id="title" 
											ref="title"
											type="text" 
											className="form-control" />
										<div id="title-validate" className="invalid-feedback">
											This field must not be empty
										</div>
									</div>
									
									<div className="form-element">
										<label className="form-label">Category</label>
										<div 
											className="form-text">
											Choose a category for yout feedback.
										</div>
										<Select id="category" ref="category">
											{
												this.state.categories.map( ( category , key ) =>
													<option key={key} value={category._id}>{category.name}</option>
												)
											}
										</Select>
										<div id="category-validate" className="invalid-feedback">
											This field must not be empty
										</div>
									</div>

									<div className="form-element">
										<label className="form-label">Feedback Detail</label>
										<div 
											className="form-text">
											Include any specific comments on what should be improved, added, etc.
										</div>
										<textarea className="form-control" id="description" ref="description"  rows="3"></textarea>
										<div id="description-validate" className="invalid-feedback">
											This field must not be empty
										</div>
									</div>

									<div className="form-footer">
										<Button
											text="Cancel"
											custom={{ 
												"className":`btn btn-three`,
												onClick:this.handlerClickBack
											}}/>
										
										<Button
											text="Add Feedback"
											custom={{ 
												"className":`btn btn-one`,
												type:"submit"	
											}}/>
									</div>
								</form>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default NewFeedback;