
import React, { Component } from 'react';

import Button from '../components/Button';
import Auth from '../controllers/auth';

import LocalStorage from '../helpers/localstorage';
import Validations  from '../helpers/validations';
import Alerts from '../helpers/alerts';
import Messages from '../helpers/messages.json';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {
			stateResponse : false,
		}
    }

	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/";
	}

	handlerSubmitLogin = (event) =>{
		event.preventDefault();
		this.setState({ stateResponse : true })

		let target = event.currentTarget;
		let validateForm = Validations.validateForm(target.elements);
		if(!validateForm.status){
			this.setState({ stateResponse : false })
			return false
		} 
		
		let data = {
			email : this.refs.email.value,
			password : this.refs.password.value
		}

		Auth.login( data ,( response ) => {
			this.setState({ stateResponse : false })
			if (response.success){
				LocalStorage.setValue('auth_token', response.data.token)
				window.location.href = '/suggestions'
			}else{
				Alerts.error(Messages[response.message]);
			}
        });
	}

	handlerClickRegister = (event) => {
		event.preventDefault();
		window.location.href = '/register';
	}

	render() {
		return (
			<div className="row ">
				{Alerts.containerAlert()}
				<div className="col-12">
					<div className="content-feedback-form">
						<div className="card" style={{ maxWidth : "45rem" , padding : "3%"}}>
							<div className="card-body content-post-comment">
								<h2 className="text-ocean" >Login </h2>
								<small>
									user : velvetround@gmail.com
								</small>
								<br />
								<small>
									pass : 12345
								</small>
								<form onSubmit={this.handlerSubmitLogin} className="needs-validation">
									<div className="form-element">
										<label className="form-label">Email</label>
										<input
											id="email" 
											type="email" 
											className="form-control" 
											ref="email" />
										<div id="email-validate" className="invalid-feedback">
											Email invalid
										</div>
									</div>
									<br />
									<div className="form-element">
										<label className="form-label">Password</label>
										<input 
											id="password"
											type="password" 
											className="form-control" 
											ref="password" />
										<div id="password-validate" className="invalid-feedback">
											Password invalid
										</div>
									</div>
								
									<br />
									<br />

									<div className="form-footer">
										<Button
											loading={this.state.stateResponse}
											text={"Login"} 
											custom={{ className :"btn btn-one btn-block" , type:"submit" }}  />	
									</div>
								</form>
								
								<div align="center" style={{marginTop:20}}>
									<a 	
										href="#"
										className="btn btn-five"
										onClick={this.handlerClickRegister} >
										Register
									</a> 
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default Login;