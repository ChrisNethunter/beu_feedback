
import React, { Component } from 'react';
import { withRouter } from "react-router";

import Icon from '../components/Icon';
import Button from '../components/Button';
import FeedBackItem from '../components/FeedBackItem';
import SuggestionsController from '../controllers/suggestions';
import CategoriesController from '../controllers/categories';
class FeedbackDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			suggestion: {}
		}
    }

	componentDidMount(){
		this.getSuggestionData()
	}

    handlerClickEditFeedback = (event) => {
		event.preventDefault();
		window.location = `/editfeedback/${this.props.match.params._id}`;
	}

	getSuggestionData = async () => {
		let data = await SuggestionsController.getSuggestions( { filter : { _id: this.props.match.params._id} })
		if (data.success){
			let current = data.data[0]
			let category = await CategoriesController.getCategories(current.category)
			if (category.success){
				current.category = category.data.name
			}
			this.setState({
				suggestion: current
			})
		}
	}

	handlerClickBack = (event) => {
		event.preventDefault();
		window.location = "/suggestions";
	}

	render() {
		return (
            <div>
				<div className="row">
					<div className="col-6">
						<Button
							text={(
								<div>
									<Icon 
										type="IconArrowLeft"
										className="icon-btn icon-btn-blue" />
									Go Back
								</div>
							)}
							custom={{ 
								"className":`btn btn-five`,
								onClick:this.handlerClickBack
							}}/>
					</div>
					<div className="col-6 ">
						<Button
							text="Edit Feeback"
							custom={{ 
								"className":`btn btn-two float-sm-end`,
								onClick:this.handlerClickEditFeedback
							}}/>
					</div>
				</div>
               	
				<div className="row content-feedback-detail">
					<div className="col-12">
						<FeedBackItem suggestion={this.state.suggestion} /> 
					</div>
				</div>

				<div className="row">
					<div className="col-12">
						<div className="card">
							<div className="card-body">
								<h2 className="text-ocean" >4 Comments </h2>
								<div className="container">
									<div className="row">
										<div className="media">
											<div className="media-left">
												<img src="http://fakeimg.pl/50x50" className="media-object" style={{width: '40px'}} />
											</div>
											<div className="media-body">
												<h4 className="media-heading title">Fahmi Arif</h4>
												<p className="komen">
													kalo bisa ya ndak usah gan biar cepet<br />
													<a href="#">reply</a>
												</p>
											</div>
										</div>
										<div className="geser">
											<div className="media">
												<div className="media-left">
													<img src="http://fakeimg.pl/50x50" className="media-object" style={{width: '40px'}} />
												</div>
											<div className="media-body">
												<h4 className="media-heading title">Fahmi Arif</h4>
												<p className="komen">
												kalo bisa ya ndak usah gan biar cepet<br />
												<a href="#">reply</a>
												</p>
											</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div className="row">
					<div className="col-12">
						<div className="card">
							<div className="card-body content-post-comment">

								<h2 className="text-ocean" >Add comment </h2>

								<form>
                    				<textarea className="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Type your comment here"></textarea>
								</form>

								<div className="row spacing-top">
									<div className="col-6">
										<p className="text-ocean-light"><small> 250 Characters left </small></p>
									</div>
									<div className="col-6">
										<Button
											text="Post comment"
											custom={{ 
												"className":`btn btn-one float-sm-end`
											}}/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

            </div>
		);
	}
}

export default withRouter(FeedbackDetail);