import  * as DataIcons from './Icons/index.js';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

function Icon ( props ) {
    const [IconLoad, setIcon] = useState(DataIcons["IconNewFeedback"]);
	useEffect(() => {
		setIcon(DataIcons[props.type])
	});
    return (
        <IconLoad {...props}></IconLoad>
    );
}

Icon.propTypes = {
    type: PropTypes.string
};

export default Icon;