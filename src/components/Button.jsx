
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
function Button(props) {
	const [loading, setLoading] = useState(false);
	useEffect(() => {
		setLoading(props.loading)
	});
	return (
		<button {...props.custom}>
			{
				loading ?
					<div className="sk-chase" style={{margin: "0 auto"}}>
						<div className="sk-chase-dot" />
						<div className="sk-chase-dot" />
						<div className="sk-chase-dot" />
						<div className="sk-chase-dot" />
						<div className="sk-chase-dot" />
						<div className="sk-chase-dot" />
					</div>
					:
					props.text 
			}
		</button>
	);
}

Button.propTypes = {
    props: PropTypes.object,
	text: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.object,
		PropTypes.func
	]),
};

export default Button;