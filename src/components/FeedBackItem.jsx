import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types';
import Icon from './Icon';
import Button from './Button';

function FeedBackItem(props){
    const [widthScreen, setwidthScreen] = useState(false);
	useEffect(() => {
        setwidthScreen(window.screen.width);
        detectWidth();
	});

    function handlerClickDetailFeedback (id) {
        window.location = `/feedbackdetail/${id}`;
    }

    function detectWidth(){
        window.addEventListener("resize", function(event) {
            setwidthScreen(window.screen.width);
        })
    }

    return (
        <div className="card card-feedback" onClick={() => handlerClickDetailFeedback(props.suggestion._id)}>
            <div className="card-body">
                <div className="content-feedback">

                    {
                        widthScreen <= 576 ?
                            <div className="row wrapper-feedback-mobile">
                                <div className="row">
                                    <div className="col-sm-12 wrapper-feedback-text">
                                        <h2 className="card-title text-ocean">
                                            { props.suggestion.title }
                                        </h2>
                                        <p className="card-text text-ocean-light">
                                            { props.suggestion.description }
                                        </p>
                                        <Button
                                            text={ props.suggestion.category } 
                                            custom={{ 
                                                className:`btn btn-seven detail-feedback-button`,
                                            }}/>
                                    </div>
                                </div>

                                <div className="row">
                                    <div className="col">
                                        <Button
                                            text={(
                                                <div className="row">
                                                    <div className="col">
                                                        <Icon type="IconArrowUp" />
                                                    </div>
                                                    <div className="col">
                                                        <b>{props.suggestion.upvotes}</b>
                                                    </div>
                                                </div>
                                            )}
                                            custom={{ 
                                                "className":`btn btn-eigth btn-up-feedback`
                                        }}/>
                                    </div>
                                    <div className="col">
                                        <h3 className="text-ocean text-end icon-comment-mobile">
                                            <Icon
                                                type="IconComments"
                                                className="icon-btn " />1
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        :
                        <div className="row wrapper-feedback" >
                        
                            <div className="col-1">
                                <Button
                                    text={(
                                        <div>
                                            <Icon type="IconArrowUp" />
                                            <br />
                                            {props.suggestion.upvotes}
                                        </div>
                                    )}
                                    custom={{ 
                                        "className":`btn btn-eigth btn-up-feedback`
                                    }}/>
                            </div>

                            <div className="col-11">
                                <h2 className="card-title text-ocean">
                                    { props.suggestion.title }
                                </h2>
                                <div className="row description-feedback">
                                    <div className="col-10">
                                        <p className="card-text text-ocean-light">
                                            { props.suggestion.description }
                                        </p>
                                    </div>
                                    <div className="col-2">
                                        <h3 className="text-ocean text-end">
                                            <Icon
                                                type="IconComments"
                                                className="icon-btn" />1
                                        </h3>
                                    </div>
                                </div>

                                <Button
                                    text={ props.suggestion.category } 
                                    custom={{ 
                                        className:`btn btn-seven detail-feedback-button`,
                                    }}/>
                            </div>
                            
                        </div>
                    }

                    
                </div>
            </div>
        </div>
    );
}

FeedBackItem.propTypes = {
    detail: PropTypes.func,
    suggestion: PropTypes.object
};

export default FeedBackItem;