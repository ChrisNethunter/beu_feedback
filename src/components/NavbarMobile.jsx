
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Icon from './Icon';


function NavbarMobile(props) {
	const [openMenu, setOpenMenu] = useState(false);
	
    function handlerClickOpenMenu(){
        let targetMenu =  document.getElementsByClassName("content-menu-mobile")[0],
        overlay =  document.getElementsByClassName("overlay")[0];
        if(openMenu){
            targetMenu.style.display = "none";
            overlay.style.display = "none";
        }else{
            targetMenu.style.display = "block";
            overlay.style.display = "block";
        }
        setOpenMenu(!openMenu);
    }

	return (
		<div className="nav-mobile-menu">
            <div className="row">
                <div className="col text-nav-mobile">
                    {
                        Object.keys(props.user).length ?
                            <h2 className="text-white">{props.user.name}</h2>
                        :
                            <h2 className="text-white">Frontend Mentor</h2>
                    }
                    <h4 className="text-white">Feedback Board</h4>
                </div>
                <div className="col">
                    <div className="menu-mobile" onClick={handlerClickOpenMenu}>
                        {
                            !openMenu ? 
                                <Icon 
                                    type="IconHamburger"
                                    className=" icon-btn-white"/>
                            :
                                <Icon 
                                    type="IconClose"
                                    className=" icon-btn-white"/>
                        }
                        
                    </div>
                </div>
            </div>
        </div>
	);
}

NavbarMobile.propTypes = {
    user: PropTypes.object,
};

export default NavbarMobile;