
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Icon from './Icon';
import Button from './Button';

function CardRoadMap(props) {
    const [data, setData] = useState({});
    const [keyData, setKey] = useState("");
    const [text, setText] = useState("");
    const [classCustom, setClassCustom] = useState("");
    const [color, setColor] = useState("");
    useEffect(() => {
        setData(props.data)
        setKey(props.keyData)
        setText(props.text)
        setClassCustom(props.classCustom)
        setColor(props.color)
    });
    return (
        <div>
            {
                Object.keys(data).length ?
                    data[keyData].map((status, key) =>
                        <div key={key} className={`card card-feedback card-feedback-roadmap ${classCustom}`} onClick={() => this.handlerClickDetailFeedback(status._id)} >
                            <div className="card-body">
                                <div className="row category-roadmap">
                                    <div className="col-1">
                                        <div className={`position-category ${color}`}></div>
                                    </div>
                                    <div className="col-5">
                                        <h4 className="text-ocean-light">{text}</h4>
                                    </div>
                                    <div className="col-12">
                                        <h3 className="text-ocean">{status.title}</h3>
                                        <p className="body-content-two">{status.description}</p>
                                        <Button
                                            text={status.category}
                                            custom={{ "className": `btn btn-seven detail-feedback-button` }} />
                                        <div className="row">
                                            <div className="col-6">
                                                <Button
                                                    text={(
                                                        <div>
                                                            <Icon type="IconArrowUp" />
                                                            {status.upvotes}
                                                        </div>
                                                    )}
                                                    custom={{
                                                        "className": `btn btn-eigth`
                                                    }} />

                                            </div>
                                            <div className="col-6">
                                                <h3 className="text-ocean text-end">
                                                    <Icon
                                                        type="IconComments"
                                                        className="icon-btn" />1
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                :
                ""
            }
        </div>
    );
}

CardRoadMap.propTypes = {
    data: PropTypes.object,
    keyData: PropTypes.string,
    text: PropTypes.string,
    classCustom: PropTypes.string,
    color:PropTypes.string
};

export default CardRoadMap;