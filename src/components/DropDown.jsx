import React, { useState } from 'react';

import Button from './Button';

export default function DropDown ( props ) {
    const [toggle, setToogle] = useState(false);
    return (
        <div className="dropdown">
            <Button
                text={ props.textButton(toggle) }
                custom={{ 
                    id:"dropdownMenuButton",
                    "data-toggle":"dropdown" ,
                    "aria-haspopup":"true", 
                    "aria-expanded":"false", 
                    "className":`btn btn-three btn-dropdown icon-btn-back ${ toggle ? "show": ""} ${ props.shadow ? "":"no-shadow" }`,
                    onClick: () => { setToogle(!toggle) }
                }}/>
            <div className={`dropdown-menu ${ toggle ? "show": ""}`} aria-labelledby="dropdownMenuButton">
                { props.children }
            </div>
        </div>
    );
}

