import Icon from './Icon';

export default function TextSort ( toggle ) {
    return(
		<p >Sort by : <b className="text-sort">Most Upvotes</b>   {
			toggle ?
					<Icon 
						type="IconArrowUp"
						className="icon-btn-left icon-btn-white" />
				:
					<Icon 
						type="IconArrowDown"
						className="icon-btn-left icon-btn-white" />
			}
		</p>
	)
}
