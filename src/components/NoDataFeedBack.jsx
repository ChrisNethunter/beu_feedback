
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Icon from './Icon';
import Button from './Button';

export default function NoDataFeedBack (props){
    function handlerClickCreateFeedback(event){
        event.preventDefault();
        window.location = "/newfeedback";
    }
    return (
        <div className="card" style={{ padding : "18%"}}>
            <div className="card-body text-center">
                <Icon type="IconIlustrationEmpty"/>
                <h1 className="card-title text-ocean">There is not feedback yet.</h1>
                <p className="card-text text-ocean-light text-not-feedbacks">
                    Got a suggestion? Found a bug that needs to be squashed?
                </p>
                <p className="card-text text-ocean-light ">
                    We love hearing about new ideas to improve our app
                </p>

                <Button
                    text={(
                        <div>
                            <Icon 
                                type="IconPlus"
                                className="icon-btn" />
                            Add Feedback
                        </div>
                    )}
                    custom={{ 
                        "className":`btn btn-one icon-btn-back`,
                        onClick:handlerClickCreateFeedback
                    }}/>
            </div>
        </div>

    );
}
