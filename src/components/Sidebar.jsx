
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import Button from './Button';

function Siderbar(props) {
    const [open, setOpen] = useState(false);
	
    useEffect(() => {
		
	});

    return (
        <div className="col-md-12 col-lg-3 col-sm-12 content-menu-mobile">
            <div className="overlay"></div>
            <div className="content-feedback-board row">

                <div className="card col-lg-11 col-md-4 margin-right-nav option-nav header-nav"  >
                    <img
                        src="#"
                        className="card-img image-header" alt="..." />

                    <div className="card-img-overlay text-white content-board-feedback">
                        {
                            Object.keys(props.user).length ?
                                <h2 className="text-white">{props.user.name}</h2>
                                :
                                <h2 className="text-white">Frontend Mentor</h2>
                        }
                        <h4>Feedback Board</h4>
                    </div>
                </div>

                <div className="card col-lg-11 col-md-4 margin-right-nav option-nav">
                    <div className="card-body">
                        <div className="col content-categories-feedback">
                            <Button
                                text={"All"}
                                custom={{
                                    id: "all",
                                    className: `btn btn-seven col ${props.activefilter == 'all' ? 'active' : ''}`,
                                    onClick: props.handlerClickFilterCategory
                                }} />
                            {
                                props.categories.map((category, key) =>
                                    <Button
                                        key={key}
                                        text={category.name}
                                        custom={{
                                            id: category._id,
                                            className: `btn btn-seven col ${props.activefilter == category._id ? 'active' : ''}`,
                                            onClick: props.handlerClickFilterCategory
                                        }} />
                                )
                            }
                        </div>
                    </div>
                </div>

                <div className="card col-lg-11 col-md-4 margin-right-nav option-nav">
                    <div className="card-body">
                        <div className="content-categories-roadmap">
                            <div className="row">
                                <div className="col">
                                    <h2 className="text-ocean" >Roadmap </h2>
                                </div>
                                <div className="col text-end">
                                    <h4><a href="#" onClick={() => window.location.href = '/roadmap'}> View </a></h4>
                                </div>
                            </div>

                            <div className="row category-roadmap">
                                <div className="col-1">
                                    <div className="position-category purple "></div>
                                </div>
                                <div className="col-5">
                                    <h4 className="text-ocean-light"> Planned</h4>
                                </div>
                                <div className="col-5">
                                    <h3 className="text-end text-ocean">{props.roadMapCount.planned}</h3>
                                </div>
                            </div>

                            <div className="row category-roadmap">
                                <div className="col-1">
                                    <div className="position-category salmon"></div>
                                </div>
                                <div className="col-6">
                                    <h4 className="text-ocean-light"> In-progress</h4>
                                </div>
                                <div className="col-4">
                                    <h3 className="text-ocean text-end">{props.roadMapCount.inProgress}</h3>
                                </div>
                            </div>

                            <div className="row category-roadmap">
                                <div className="col-1">
                                    <div className="position-category bluelight"></div>
                                </div>
                                <div className="col-5">
                                    <h4 className="text-ocean-light"> Live</h4>
                                </div>
                                <div className="col-5">
                                    <h3 className="text-ocean text-end">{props.roadMapCount.live}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="card col-lg-11 col-md-12">
                    <div className="card-body">
                        <Button
                            text="Log out"
                            custom={{
                                className: `btn btn-three btn-block btn-logout`,
                                onClick: props.handlerClickLogOut
                            }} />
                    </div>
                </div>

            </div>
        </div>
    );
}

Siderbar.propTypes = {
    user: PropTypes.object,
    categories: PropTypes.array,
    roadMapCount: PropTypes.object,
    activefilter : PropTypes.string,
    handlerClickFilterCategory : PropTypes.func,
    handlerClickLogOut : PropTypes.func,
};

export default Siderbar;