import { ReactComponent as IconArrowDown } from './icon-arrow-down.svg';
import { ReactComponent as IconArrowLeft } from './icon-arrow-left.svg';
import { ReactComponent as IconArrowUp } from './icon-arrow-up.svg';
import { ReactComponent as IconCheck } from './icon-check.svg';
import { ReactComponent as IconClose } from './icon-close.svg';
import { ReactComponent as IconComments } from './icon-comments.svg';
import { ReactComponent as IconEditFeedback } from './icon-edit-feedback.svg';
import { ReactComponent as IconHamburger } from './icon-hamburger.svg';
import { ReactComponent as IconPlus } from './icon-plus.svg';
import { ReactComponent as IconSuggestion } from './icon-suggestions.svg';
import { ReactComponent as IconIlustrationEmpty } from './illustration-empty.svg';
import { ReactComponent as IconNewFeedback } from './icon-new-feedback.svg';

export {
    IconArrowDown,
    IconArrowLeft,
    IconArrowUp,
    IconCheck,
    IconClose,
    IconComments,
    IconEditFeedback,
    IconHamburger,
    IconPlus,
    IconSuggestion,
    IconIlustrationEmpty,
    IconNewFeedback
};