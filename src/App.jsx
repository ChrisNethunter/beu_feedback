import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
//Layouts
import MainLayout from './layouts/MainLayout';
import FeedbackLayout from './layouts/FeedbackLayout';
//Layouts

//Pages
import Home from './pages/Home.jsx';
import Login from './pages/Login.jsx';
import Register from './pages/Register.jsx';
import Suggestions from './pages/Suggestions.jsx';
import EditFeedback from './pages/EditFeedback.jsx';
import FeedbackDetail from './pages/FeedbackDetail.jsx';
import RoadMap from './pages/RoadMap.jsx';
import NewFeedback from './pages/NewFeedback.jsx';
import NotFound from './pages/NotFound.jsx';
//Pages
import 'sweetalert2/src/sweetalert2.scss'

const AppRoute = ({ component:Component , layout:Layout ,...rest  } ) => (
    <Route {...rest} render={props=> (
        <Layout > <Component {...props}></Component></Layout>
    )}></Route>
);
export default class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <BrowserRouter basename="/">
                <Switch>
                    <AppRoute path="/" exact layout={MainLayout} component={() => <Login />} />
                    <AppRoute path="/register" exact layout={MainLayout} component={() => <Register />} />
                    <AppRoute path="/items" exact layout={FeedbackLayout} component={() => <Home />} />
                    <AppRoute path="/suggestions" exact layout={FeedbackLayout} component={() => <Suggestions />} />
                    <AppRoute path="/login" exact layout={MainLayout} component={() => <Login />} />
                    <AppRoute path="/newfeedback" exact layout={FeedbackLayout} component={() => <NewFeedback />} />
                    <AppRoute path="/editfeedback/:_id" exact layout={FeedbackLayout} component={() => <EditFeedback />} />
                    <AppRoute path="/feedbackdetail/:_id" exact layout={FeedbackLayout} component={() => <FeedbackDetail />} />
                    <AppRoute path="/roadmap" exact layout={FeedbackLayout} component={() => <RoadMap />} />
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
}
