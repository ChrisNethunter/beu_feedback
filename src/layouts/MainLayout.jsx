import React from "react";
import Utils  from '../helpers/utils';

export default class MainLayout extends React.Component {
    constructor(props) {
		super(props);
		this.state = {};
    }

    componentDidMount(){
        Utils.validateSession();
    }

    render() {
        return (
            <div className="content-layout container">
                <React.Fragment>    
                    {/*  <NavbarNode/> */}
                    { this.props.children }
                    {/*    <h1> Footer </h1> */}
                </React.Fragment>
            </div>            
        )
    }
}
