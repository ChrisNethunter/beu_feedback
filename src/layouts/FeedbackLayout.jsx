import React from "react";

import LocalStorage from '../helpers/localstorage';
import Alerts from '../helpers/alerts';

import Auth from '../controllers/auth';

export default class FeedbackLayout extends React.Component {
    constructor(props) {
		super(props);
		this.state = {};
    }
    
    componentDidMount(){
        this.validateDataUser()
    }

    validateDataUser(){
        Auth.getUser( ( response ) => {
			if(!response.success || Object.keys(response.data).length == 0 ){
                LocalStorage.clearLocalStorage();
                window.location.href="/";
            }
        });
    }

    render() {
        return (
            <div className="content-layout container">
                { Alerts.containerAlert() }
                <React.Fragment>    
                    { this.props.children }
                </React.Fragment>
            </div>            
        )
    }
}
