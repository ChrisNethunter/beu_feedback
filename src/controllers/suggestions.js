const request = require("../helpers/request.js").default;

export default new class SuggestionsController{
    getSuggestions = ( filter ) => {
        return new Promise( ( resolve ) =>{
            request.postJSON('/api/suggestions/getSuggestions', filter ).then((response) => {
                return resolve(response);
            });
        });
    }

    getDataRoadMap = () => {
        return new Promise( ( resolve ) =>{
            request.postJSON('/api/suggestions/getDataRoadMapCount').then((response) => {
                return resolve(response);
            });
        });
    }

    createSuggestion = ( data , callback ) => {
        request.postJSON('/api/suggestions/createSuggestion', data ).then((response) => {
            return callback(response);
        });
    }

    editSuggestion = ( data , callback ) => {
        request.postJSON('/api/suggestions/editSuggestion', data ).then((response) => {
            return callback(response);
        });
    }

    deleteSuggestion = ( data , callback ) => {
        request.postJSON('/api/suggestions/delteSuggestion', data ).then((response) => {
            return callback(response);
        });
    }
}()