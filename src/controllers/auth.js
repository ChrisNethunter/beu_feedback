const request = require("../helpers/request.js").default;

export default new class Auth{
    login = (data, callback) => {
        request.postJSON('/api/login/auth',data).then((response) => {
            return callback(response);
        });
    }

    register = (data, callback) => {
        request.postJSON('/api/login/register',data).then((response) => {
            return callback(response);
        });
    }
    
    getUser = (callback) => {
        request.postJSON('/api/user/getUser').then((response) => {
            return callback(response);
        });
    }
}();