const request = require("../helpers/request.js").default;

export default new class CategoriesController{
    getCategories = (categories_id) => {
        return new Promise( ( resolve ) =>{
            request.postJSON('/api/categories/getCategorie', { categories_id: categories_id } ).then((response) => {
                return resolve(response);
            });
        });
    }

    getAllCategories = () => {
        return new Promise( ( resolve ) =>{
            request.postJSON('/api/categories/getAllCategories').then((response) => {
                return resolve(response);
            });
        });
    }

}()