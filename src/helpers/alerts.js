import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default new class Alerts {
    constructor() {}
    
    success(message){
        toast(message, {
            position: "top-right",
            autoClose: 5000,
            closeOnClick: true,
            pauseOnHover: true,
            type:"success",
            progress: undefined,
        });
    }

    error(message){
        toast(message, {
            position: "top-right",
            autoClose: 5000,
            closeOnClick: true,
            pauseOnHover: true,
            type:"error",
            progress: undefined,
        });
    }

    warning(message){
        toast(message, {
            position: "top-right",
            autoClose: 5000,
            closeOnClick: true,
            pauseOnHover: true,
            type:"warning",
            progress: undefined,
        });
    }

    info(message){
        toast(message, {
            position: "top-right",
            autoClose: 5000,
            closeOnClick: true,
            pauseOnHover: true,
            type:"info",
            progress: undefined,
        });
    }

    containerAlert(){
        return(
            <ToastContainer
                position="top-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover/>
        )
    }

}
