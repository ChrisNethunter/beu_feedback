import LocalStorage from '../helpers/localstorage';

export default new class Utils {
    constructor() {}
    
    validateSession(){
        if(LocalStorage.getValue('auth_token') ){
			window.location.href = '/suggestions';
		}
    }

    validateResponse( getData ){
        if(getData.success){
			return {
                success : true,
                response : getData.data 
            }
		}else{
            return {
                success : false,
                response : getData.message
            }
        }
    }
}

