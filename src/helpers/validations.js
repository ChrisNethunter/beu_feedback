export default new class Validations {
    constructor() {
        this.validations = {
            number : 'validateNumber',
            url : 'validateUrl',
            email : 'validateEmail',
            text : 'validateEmpty',
            password : 'validatePassword',
            select : 'validateEmpty',
            time : 'validateTime',
            date : 'validateDate',
            color : 'validateEmpty',
            checkbox : 'validateChecked'
        }
        this.class_invalid = 'is-invalid';
        this.class_valid = 'is-valid';

    }
    validateForm ( target ) {
        for ( let element  of  target ){
			if( element.type !== 'submit'){
                this.resetStylealidateError(element);
                let targetToValidate = element.value;

                if(element.type == 'checkbox'){
                    targetToValidate = element;
                    if(!element.checked){
                        return false
                    }
                }else{
                    if( element.value === ''  ){
                        this.styleValidateError(element);
                        return false
                    }
                    
                    if(this.validations[element.type]){
                        let execute_validation = eval(`this.${this.validations[element.type]}('${targetToValidate}')`);
                        if(!execute_validation ){
                            this.styleValidateError(element);
                            return false
                        }
                    }
                    
                }
			}
        }
        return {
            status : true
        }
    }

    styleValidateError( element ){
        element.classList.add(this.class_invalid);
        document.getElementById(`${element.id}-validate`).style.display = "";
    }

    styleValidateSucces( element ){
        element.classList.add(this.class_valid);
    }

    resetStylealidateError( element){
        element.classList.remove(this.class_invalid);
    }

    validateEmpty( element){
        if(element === ''){
           return false 
        }else{
            return true 
        }
    }

    validateNumber( element){
        if(isNaN(element)){
           return false 
        }else{
            return true 
        }
    }

    validateEmail( email ) {
        const expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ( expr.test(email) ){
            return true
        }else{
            return false
        }
    }

    validateUrl( url ) {    
        const expr = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        if (expr.test(url)){
            return true;
        }else{
            return false;
        }
    }

    validateCheck( checkhtml){
        if (checkhtml.checked) {  
          return true
        }else{
          return false
        }
    }

    validateTime( time ) {
        const expr = /\d+:\d+/;
        if(expr.test(time)){
            return true 
        }else{
            return false
        }   
    }

    validateDate(date){
        var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(date);
        if (matches == null) {
            return false;
        }else{
            return true; 
        } 
    }

    validatePassword (pass){
        if(pass){
            return true 
        }else{
            return false
        }
    }

    validateChecked (check){
        if(check.checked){
            return true 
        }else{
            return false
        }
    }

}

