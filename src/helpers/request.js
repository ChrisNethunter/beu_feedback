import LocalStorage from '../helpers/localstorage';
const baseURL = process.env.NODE_ENV === "development" ? "http://localhost:8000" : "https://production.com";

export default new class Request {

    constructor(props) {
        this.loading = new CustomEvent("loading", {
            "detail": "Start request"
        });
        this.ended = new CustomEvent("ended", {
            "detail": "Finish Request"
        });
    }

    postJSON(url, data) {
        if(process.env.NODE_ENV === "development"){
            if(url.search(':') == -1) url = baseURL + url;
        }
        return new Promise(function(resolve, reject) {
            fetch(url, {
                headers: {
                    'Accept': 'application/json',
                    'x-access-token': LocalStorage.getValue('auth_token'),
                    'Content-Type': 'application/json',
                    'access-method': 'api-json'
                    //'Access-Control-Allow-Origin:':'*'
                },
                credentials: 'same-origin',
                method: "POST",
                body: JSON.stringify(data)
            }).then(function(response) {
                response.json().then(function(res) {
                    resolve(res);
                }).catch(function(res) {
                    reject(res);
                });
            }).catch(function(err) {
                reject(err);
            });
        });
    }

    getJSON(url) {
        var me = this;
        document.dispatchEvent(this.loading);
        return new Promise(function(resolve, reject) {
            fetch(url, {
                headers: {
                    'access-method': 'api-json'
                },
                credentials: "same-origin"
            }).then(function(response) {
                response.json().then(function(res) {
                    document.dispatchEvent(me.ended);
                    resolve(res);
                }).catch(function(res) {
                    document.dispatchEvent(me.ended);
                    reject(res);
                });
            });
        });
    }

    upload(url, file) {
        var me = this;
        document.dispatchEvent(this.loading);
        return new Promise(function(resolve, reject) {
            var data = new FormData();
            data.append('file', file)
            data.append('username', LocalStorage.getValue('username'));

            fetch(url, {
                method: 'POST',
                body: data,
                headers: {
                    'x-access-token': LocalStorage.getValue('auth_token'),
                    'access-method': 'api-json'
                },
                credentials: "same-origin"
            }).then(function(response) {
                response.json().then(function(res) {
                    document.dispatchEvent(me.ended);
                    resolve(res);
                }).catch(function(res) {
                    document.dispatchEvent(me.ended);
                    reject(res);
                });
            });
        })
    }

}
