const daoUsers = require('../dao/users.js');
const utils = require('../helpers/utils.js');

/**
 * @class User
 * @memberof Routes
 *
 *  @ classdesc This class contains the auth routes of user
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires    daoUsers
 * @requires    utils
 *
 */

class User {
    constructor() {}
    async getUser(req, res) {
        let getDataUser = await daoUsers.readUserByUserId(req.session._id);
        if(Object.keys(getDataUser).length == 0){
            return utils.sendError(res,"not_session_found");
        }
        var data = {
            email: getDataUser.email,
            username : getDataUser.username,
            name : getDataUser.name,
            image : getDataUser.image,
        };
        return utils.sendSuccess(res,data);
    }
}

module.exports = User;