const daoSuggestions = require('../dao/suggestions.js');
const daoCategory = require('../dao/categories.js');
const utils = require('../helpers/utils.js');

/**
 * @class Suggestions
 * @memberof Routes
 *
 *  @ classdesc This class contains the auth routes of suggestions
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires    daoSuggestions
 * @requires    utils
 *
 */

class Suggestions {
    constructor(){}
    
    async getSuggestions(req, res) {
        if(!req.body.filter){
            return utils.sendError(res, "filter-no-found");
        }
        let getData = await daoSuggestions.getSuggestions(req.body.filter);
        if(getData.error){
            return utils.sendError(res, getData.error);
        }
        return utils.sendSuccess(res, getData.data);
    }

    async getDataRoadMapCount(req, res) {
        let dataCountRoadMap = {
            live:0,
            planned:0,
            inProgress:0,
            suggestions:0,
            data: {}
        }
        
        let getDataLive = await daoSuggestions.getSuggestions({status:"live"});
        if(getDataLive.error){
            return utils.sendError(res, getDataLive.error);
        }
        dataCountRoadMap.live = getDataLive.data.length;

        let data_parce_live = await this.parceCategoryData(getDataLive)
        dataCountRoadMap.data['live'] = data_parce_live

        let getDataPlanned = await daoSuggestions.getSuggestions({status:"planned"});
        if(getDataPlanned.error){
            return utils.sendError(res, getDataPlanned.error);
        }
        dataCountRoadMap.planned = getDataPlanned.data.length;
        let data_parce_planned = await this.parceCategoryData(getDataPlanned)
        dataCountRoadMap.data['planned'] = data_parce_planned

        let getDataInProgress = await daoSuggestions.getSuggestions({status:"in-progress"});
        if(getDataInProgress.error){
            return utils.sendError(res, getDataInProgress.error);
        }
        dataCountRoadMap.inProgress = getDataInProgress.data.length;
        let data_parce_inprogress = await this.parceCategoryData(getDataInProgress)
        dataCountRoadMap.data['inProgress'] = data_parce_inprogress

        let getDataSuggestions = await daoSuggestions.getSuggestions({status:"suggestion"});
        if(getDataSuggestions.error){
            return utils.sendError(res, getDataSuggestions.error);
        }
        dataCountRoadMap.suggestions = getDataSuggestions.data.length;
        let data_parce_suggestions = await this.parceCategoryData(getDataSuggestions.data)
        dataCountRoadMap.data['suggestions'] = data_parce_suggestions

        return utils.sendSuccess(res, dataCountRoadMap);
    }
    
    parceCategoryData(getData){
        return new Promise( async (resolve) => {
            let data = []
            for (let item in getData.data){
                let category = await daoCategory.getCategories({_id: getData.data[item].category})
                let current = getData.data[item]
                current['category'] = category.data.name
                data.push(current)
            }
            return resolve(data)
        })
    }

    async createSuggestion(req, res) {
        let data = {
            title: req.body.title,
            category: req.body.category,
            description: req.body.description
        }

        let good_params = true;
        for (var i in data) {
            if (!data[i]) {
                good_params = false;
                break;
            }
        }

        if (!good_params) return utils.sendError(res, "missing_paramsss");
        data.user = req.session._id;
        
        let statusCreateSuggestion = await daoSuggestions.createSuggestion(data);
        if (statusCreateSuggestion.error) return utils.sendError(res, statusCreateSuggestion.error);
        
        return utils.sendSuccess(res, true);
    }

    async editSuggestion(req, res) {
        let data = {
            _id: req.body._id,
			title : req.body.title,
			category : req.body.category,
			status : req.body.status,
			description : req.body.description,
        }

        let good_params = true;
        for (var i in data) {
            if (!data[i]) {
                good_params = false;
                break;
            }
        }

        if (!good_params) return utils.sendError(res, "missing_paramsss");
        delete data['_id']
        
        let statusCreateSuggestion = await daoSuggestions.editSuggestion(req.body._id, data);
        if (statusCreateSuggestion.error) return utils.sendError(res, statusCreateSuggestion.error);
        
        return utils.sendSuccess(res, true);
    }
    
    async delteSuggestion(req, res) {
        let data = {
            _id: req.body._id,
        }

        let good_params = true;
        for (var i in data) {
            if (!data[i]) {
                good_params = false;
                break;
            }
        }

        if (!good_params) return utils.sendError(res, "missing_paramsss");
        
        let statusDeleteSuggestion = await daoSuggestions.deleteSuggestion(data);
        if (statusDeleteSuggestion.error) return utils.sendError(res, statusDeleteSuggestion.error);
        
        return utils.sendSuccess(res, true);
    }
}


module.exports = Suggestions;