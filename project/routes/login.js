const Users = require('../dao/users.js');
const AuthMiddleware = require('../middleware/AuthMiddleware.js')
const { passwordVerify } = require('../helpers/generatePassword.js');
const utils = require('../helpers/utils.js');

/**
 * @class Login
 * @memberof Routes
 *
 *  @ classdesc This class contains the auth routes of user
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires    daoCategories
 * @requires    AuthMiddleware
 * @requires    passwordVerify
 * @requires    utils
 *
 */
class Login {
    constructor() {}
    async auth(req, res) {
        if ('email' in req.body && 'password' in req.body) {
            let email = req.body.email;
            let password = req.body.password;
            let get_user = await Users.getByEmail(email);

            if (Object.keys(get_user).length > 0) {
                let validate_password = await passwordVerify(password, get_user.password)
                if (validate_password) {
                    AuthMiddleware.GenerateTokenSession(get_user._id).then(response => {
                        if (Object.keys(response)[0] == 'error'){
                            return utils.sendError( res, response);
                        }
                        return utils.sendSuccess( res, response);
                    })
                } else {
                    return utils.sendError(res,'password-error');
                }
            } else {
                return utils.sendError(res,'user-not-exist');
            }
        } else {
            return utils.sendError(res , 'not-valid-parameters');
        }
    }

    async register(req, res) {
        let data = {
            email: (req.body.email || "").toLowerCase(),
            name: req.body.name,
            password: req.body.password
        }

        let good_params = true;
        for (var i in data) {
            if (!data[i]) {
                good_params = false;
                break;
            }
        }

        if (!good_params) return utils.sendError(res, "missing_paramsss");
        if (data.password.length < 6) return utils.sendError(res, "invalid_password_length");

        let validate_user_register = await Users.getByEmail(data.email)

        if (Object.keys(validate_user_register).length > 0) {
            return utils.sendError(res, "user_already_exist");
        }

        let statusCreateUser = await Users.createUser(data);
        if (statusCreateUser.error) return utils.sendError(res, statusCreateUser.error);
        
        return utils.sendSuccess(res, true);
    
    }
}


module.exports = Login;