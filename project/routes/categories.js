const daoCategories = require('../dao/categories.js');
const utils = require('../helpers/utils.js');
const mongodb = require('../helpers/mongodb.js');

/**
 * @class Categories
 * @memberof Routes
 *
 * @ classdesc This class contains the category routes of categories
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires    daoCategories
 *
 */
class Categories {
    constructor(){}
    
    async getCategorie(req, res) {
        if (req.body.categories_id){
            let getData = await daoCategories.getCategories({_id: mongodb.objectId(req.body.categories_id)});
            if(getData.error){
                return utils.sendError(res, getData.error);
            }
            return utils.sendSuccess(res, getData.data);
        }else{
            return utils.sendError(res, 'missing_params');
        }
    }
    
    async getAllCategories(req, res) {
        let getData = await daoCategories.getAllCategories({});
        if(getData.error){
            return utils.sendError(res, getData.error);
        }
        return utils.sendSuccess(res, getData.data);
    }
}


module.exports = Categories;