const { ValidateSessionAndToken } = require('./AuthMiddleware');
const mongodb = require('../helpers/mongodb.js');
class SessionChecker {
    constructor(){
        this.routesValidateSession  = [
            "auth",
            "register"
        ]
        this.header_access = ['x-access-token'];
    }
   
    validateSession = async (req, res, next) => {
        let route = req.url.split("/")[req.url.split("/").length -1]
        let delete_parameters = route.split("?")
        if (delete_parameters.length > 0){
            route = delete_parameters[0]
        }

        if (this.routesValidateSession.indexOf(route) !== -1) {
            return next();
        }
        
        if (req.url.indexOf('/api/') == 0) {
            let validation_headers = false;
        
            for (let index = 0; index < this.header_access.length; index++) {
                validation_headers = req.headers.hasOwnProperty(this.header_access[index]);
                if (validation_headers) break;
            }

            if (!validation_headers && this.routesValidateSession.indexOf(route) !== -1) {
                return res.status(403).send({
                    success: false,
                    message: 'unexpected_headers'
                });
            }

            let validateroute = await this.validateRote(req);
            if (!validateroute.success) return res.status(403).send(validateroute);
            req.session._id = mongodb.objectId(validateroute.data);
            return next();
        }
        return next();
    }

    validateRote(req){
        return new Promise(async (resolve) => {
            let current_token;
    
            for (let head in req.headers) {
                if (head.indexOf('postman') >= 0) continue;
                if (head.indexOf('token') > 0) {
                    current_token = req.headers[head]
                }
            }

            const decoded = await ValidateSessionAndToken(current_token);

            if (decoded.session) {
                return resolve({
                    success: true,
                    data: decoded.user_id,
                });
               
            } else return resolve({
                success: false,
                message: 'decoded_user_token_error'
            });
        });
    }


};

module.exports = new SessionChecker();