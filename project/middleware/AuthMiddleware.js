const jwt = require('jsonwebtoken');
const { v4: uuidv4 } = require('uuid');
const randomstring = require("randomstring");
const AuthUsers = require('../dao/authuser.js')
const Encription = require('../helpers/driver_encription.js')

class AuthMiddleware {
    generateRandomHash = () => {
        return randomstring.generate(10);
    }

    addDays = (date, days) => {
        let new_date = new Date(date)
        new_date.setDate(new_date.getDate() + days);
        return new Date(new_date).getTime();
    }

    GenerateTokenSession(userId) {
        return new Promise(async(resolve) => {
            userId = userId.toString()
            let expired_token = '5d';
            let primary_cript_hash = this.generateRandomHash()

            let user_uuid = String(uuidv4())
            let expire_date = this.addDays(new Date(), 2)

            let encrypted_uuid = await Encription.encrypt(user_uuid, primary_cript_hash)
            let encrypted_user_id = await Encription.encrypt(userId, primary_cript_hash)

            let data = {
                uuid: encrypted_uuid,
                user_id: encrypted_user_id,
                expence_date: expire_date
            }

            let encrypted_payload = {
                payload: JSON.stringify(data)
            }

            jwt.sign(encrypted_payload, primary_cript_hash, {
                algorithm: "HS512",
                expiresIn: expired_token
            }, async function(err, token) {
                if (err) return resolve({ error: "Error generating token" });

                AuthUsers.removeAuth(userId).then(response => {
                    AuthUsers.registerAuth({
                        user: userId,
                        uuid: user_uuid,
                        hash: primary_cript_hash,
                        token: token
                    }).then(response => {
                        if (response.data.length > 0) {
                            return resolve({ token });
                        } else {
                            return resolve({ error: response });
                        }
                    })
                })
            });
        });
    }

    ValidateSessionAndToken = (token) => {
        return new Promise((resolve) => {
            AuthUsers.getAuth({token: token}).then(response => {
                if (Object.keys(response.data).length > 0) {
                    let current = response.data;

                    jwt.verify(token, current.hash, {
                        algorithms: ["HS512"]
                    }, async (err, decodedToken) => {
                        if (err) {
                            resolve({
                                error: "no session"
                            });
                        } else {
                            let data = JSON.parse(decodedToken.payload)
                            let user_id = await Encription.decrypt(data.user_id, current.hash)
                            let uuid = await Encription.decrypt(data.uuid, current.hash)
                         
                            AuthUsers.getValidAuth(user_id, uuid).then(user_response => {
                                if (Object.keys(user_response.data).length > 0) {
                                    return resolve({ session: true, user_id: user_id })
                                } else {
                                    return resolve({
                                        error: "token invalid"
                                    })
                                }
                            })
                        }
                    });

                } else {
                    return resolve({
                        error: "no session"
                    })
                }
            })
        })
    }
}


module.exports = new AuthMiddleware();