var mongoose = require("mongoose");

var suggestions = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    user: {
        type:mongoose.Schema.Types.ObjectId,
        required: false,
    },
    upvotes: {
        type: Number,
        required: false,
        default: 0,
    },
    status: {
        type: String,
        required: false,
        default: "suggestion",
    },
    description: {
        type: String,
        required: true,
    },
    date_created: {
        type: Date,
        default: new Date().getTime(),
    }
});

module.exports = mongoose.model("suggestions", suggestions);
