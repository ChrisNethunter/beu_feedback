var mongoose = require("mongoose");

var categories = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    date_created: {
        type: Date,
        default: new Date().getTime(),
    }
});

module.exports = mongoose.model("categories", categories);