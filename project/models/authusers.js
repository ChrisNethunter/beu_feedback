var mongoose = require('mongoose');

const addDays = (date, days) => {
    let new_date = new Date(date)
    new_date.setDate(new_date.getDate() + days);
    return new Date(new_date).getTime();
}

var auth_user = new mongoose.Schema({
    user: {
        type: String,
        required: true,
    },

    uuid: {
        type: String,
        required: true,
        default: null
    },

    date_created: {
        type: Number,
        required: false,
        default: new Date().getTime()
    },

    date_expence: {
        type: Number,
        required: false,
        default: addDays(new Date(), 5)
    },

    hash: {
        type: String,
        required: true,
        default: null
    },

    token: {
        type: String,
        required: true,
        default: null
    },
});

module.exports = mongoose.model('authusers', auth_user);