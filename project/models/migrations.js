var mongoose = require('mongoose');

var migrations = new mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('migrations', migrations);