var mongoose = require("mongoose");

var users = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        default: null,
        unique: true,
    },
    password: {
        type: String,
        required: true,
        default: null,
    },
    image: {
        type: String,
        required: false,
        default: null,
    },
    username: {
        type: String,
        required: false,
        default: null,
    },
    name: {
        type: String,
        required: false,
        default: null,
    },
    date_created: {
        type: Date,
        default: new Date().getTime(),
    }
});

module.exports = mongoose.model("users", users);