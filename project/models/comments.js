var mongoose = require("mongoose");

var comments = new mongoose.Schema({
    content: {
        type: String,
        required: true,
    },
    is_parent: {
        type: Boolean,
        required: true,
        default: true
    },
    child_of: {
        type: mongoose.Schema.Types.ObjectId,
        required: false
    },
    suggesion: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    user: {
        type:mongoose.Schema.Types.ObjectId,
        required: true,
    },
    date_created: {
        type: Date,
        default: new Date().getTime(),
    }
});

module.exports = mongoose.model("comments", comments);