const mongodb = require('../helpers/mongodb.js');

/**
 * @class Comments
 * @memberof Dao
 *
 * @classdesc This class contains the crud functions for the users commets suggesions
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires mongodb
 */
class Comments {
    constructor() {
        this.collection = "comments"
    }

    getChild = async data => {
        let child_comments = await this.getChildCommets(data);
        data['childs'] = []
        
        if(child_comments.length){
            for (let child of child_comments){
                data['childs'].push(await getChild(child));
            }   
        }

        return data
    }
    
    getComment(filter) {
        return new Promise(async (resolve) => {
            filter['is_parent'] = true
            mongodb.searchOrderBy( this.collection, filter, sort, async (error, child_comments) => {
                if (child_comments.length>0){
                    for (let child in child_comments){
                        child_comments[child] = this.getChild(child)
                    }
                }
                return resolve(child_comments)
            })
        })
    }

    getChildCommets(parent){
        let sort = {
            date_created: -1
        }
        return new Promise( (resolve) => {
            let consult_child = {
                is_parent: false,
                child_of: parent._id,
                suggesion: parent.suggesion
            }
            mongodb.searchOrderBy( this.collection, consult_child, sort, async (error, child_comments) => {
                return resolve (child_comments)
            })
        })
    }

    createComment(data) {
        return new Promise(async (resolve) => {
            mongodb.save(this.collection, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                })
            });
        })
    }

    editComment(id, data) {
        return new Promise(async (resolve) => {
            mongodb.updateCollection(this.collection,  {_id: id}, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                })
            });
        })
    }
}

module.exports = new Comments();