const mongodb = require('../helpers/mongodb.js');
const { generatePassword } = require('../helpers/generatePassword.js');

/**
 * @class Categories
 * @memberof Dao
 *
 * @classdesc This class contains the crud functions for suggestions
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires mongodb
 * @requires generatePassword
 * 
 */

class User {
    constructor() { }

    createUser(user) {
        return new Promise(async (resolve) => {
            user.password = await generatePassword(user.password);

            mongodb.save('users', user, async function (error, new_user) {
                return resolve({
                    error: error,
                    user: new_user
                })
            });
        })
    }

    getByEmail(email) {
        return new Promise(async (resolve, reject) => {
            let options = {
                email: email
            };

            mongodb.find('users', options, (err, response) => {
                return resolve(response)
            });
        })
    }

    readUserByUserId(_id) {
        return new Promise(async (resolve, reject) => {
            mongodb.find("users", { _id: _id, }, (err, user) => {
                if (err || !user) {
                    return resolve({});
                }
                
                return resolve(user);
            }
            );
        });
    }
}


module.exports = new User();