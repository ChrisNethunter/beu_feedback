const mongodb = require('../helpers/mongodb.js');

/**
 * @class Categories
 * @memberof Dao
 *
 * @classdesc This class contains the crud functions for categories
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires mongodb
 */

class Categories {
    constructor() {
        this.collection = "categories"
    }

    getCategories(query) {
        return new Promise(async (resolve) => {
            mongodb.find( this.collection, query, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    getAllCategories(query) {
        return new Promise(async (resolve) => {
            mongodb.search( this.collection, query, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }
}

module.exports = new Categories();