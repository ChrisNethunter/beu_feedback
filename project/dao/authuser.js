const mongodb = require('../helpers/mongodb.js');

/**
 * @class AuthUsers
 * @memberof Dao
 *
 * @classdesc This class contains the user authentication functions
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires mongodb
 */

class AuthUsers {
    constructor() {
        this.collection = "authusers"
    }
    
    registerAuth(data) {
        return new Promise(async (resolve) => {
            mongodb.save( this.collection, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    removeAuth(user) {
        return new Promise(async (resolve) => {
            mongodb.remove( this.collection, {user: user}, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    removeAllAuth() {
        return new Promise(async (resolve) => {
            mongodb.remove( this.collection, {}, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    getAuth(query) {
        return new Promise(async (resolve) => {
            mongodb.find( this.collection, query, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    getValidAuth(userId, user_uuid) {
        let options = {
            user: userId,
            uuid: user_uuid,
            date_expence: {
                $gte: new Date().getTime()
            }
        }
        return new Promise(async (resolve) => {
            mongodb.find( this.collection, options, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }
}

module.exports = new AuthUsers();