const mongodb = require('../helpers/mongodb.js');

/**
 * @class Categories
 * @memberof Dao
 *
 * @classdesc This class contains the crud functions for suggestions
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires mongodb
 */
class Suggestions {
    constructor() {
        this.collection = "suggestions"
    }
    getSuggestions(filter) {
        let sort = {
            date_created: -1
        }
        return new Promise(async (resolve) => {
            mongodb.searchOrderBy( this.collection, filter, sort, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                });
            });
        })
    }

    createSuggestion(data) {
        return new Promise(async (resolve) => {
            mongodb.save(this.collection, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                })
            });
        })
    }

    editSuggestion(id, data) {
        return new Promise(async (resolve) => {
            mongodb.updateCollection(this.collection, {_id: id}, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                })
            });
        })
    }

    deleteSuggestion(data) {
        return new Promise(async (resolve) => {
            mongodb.remove(this.collection, data, async function (error, response) {
                return resolve({
                    error: error,
                    data: response
                })
            });
        })
    }
}

module.exports = new Suggestions();