const bcrypt = require("bcryptjs");
const passwordHash = require('password-hash');

const generatePassword = (password) => {
    return new Promise((resolve) => {
        const saltRounds = 10;
        bcrypt.genSalt(saltRounds, function(err, salt) {
            bcrypt.hash(password, salt, function(err, hash) {
                return resolve(hash);
            });
        });
    })
}

const passwordVerify = (password, bd_hash) => {
    return new Promise((resolve) => {
        bcrypt.compare(password, bd_hash, function(err, pass_verify) {
            let node_verify = passwordHash.verify(password, bd_hash);
            return resolve(pass_verify || node_verify)
        });
    })
}

module.exports = {
    generatePassword,
    passwordVerify
}