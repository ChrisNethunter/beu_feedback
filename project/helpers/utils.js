/**
 * @class Utils
 * @memberof Helpers
 *
 * @classdesc This class contains dynamic functions for general use
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 */
class Utils {
    constructor(){}
    
    sendError(res, error) {
        res.status(200).json({
            success: false,
            message: error
        })
    }

    sendSuccess(res, data) {
        res.status(200).json({
            success: true,
            data: data
        })
    }
};


module.exports = new Utils();