var mongoose = require('mongoose');
var db = mongoose.connection;
var fs = require('fs');
const logger = require('./logger')
var models = {};
const CURRENT_ENV = process.env.WEI_ENVIRONMENT || 'development';
var environment = config[CURRENT_ENV];

function mongodb() {
    fs.readdirSync(__dirname.replace('helpers', 'models')).forEach(function(file) {
        var moduleName = file.split('.')[0];
        models[moduleName] = require(__dirname.replace('helpers', 'models') + '/' + moduleName);
    });
    db.on('error', function(error) {
        logger.error("connecting mongo error ", error);
    });
    db.once('open', function() {
        logger.debug("Connection successfuly to mongo database " + environment.db_database);
    });

    let uri;

    if (CURRENT_ENV == "development") {
        uri = 'mongodb://' + environment.db_host + '/' + environment.db_database;
    } else {
        uri = 'mongodb://' + environment.db_host + '/' + environment.db_database;
    }

    console.log({ CURRENT_ENV });
    console.log({ uri });

    mongoose.connect(uri);
    mongoose.Promise = global.Promise;
};

mongodb.prototype.find = function(collection, params, callback) {
    models[collection].findOne(params).lean().exec(function(err, doc) {
        if (err) {
            return callback(err, {});
        }
        return callback(null, doc || {});
    })
}

mongodb.prototype.search = function(collection, params, callback) {
    models[collection].find(params, function(err, doc) {
        if (err) {
            return callback(err, []);
        }
        return callback(null, doc);
    })
}

mongodb.prototype.searchOrderBy = function (collection, params, sort, callback) {
    models[collection].find(params).sort(sort).exec(function (err, orderDocs) {
        if (err) {
            return callback(err, []);
        }
        return callback(null, orderDocs);
    });
}

mongodb.prototype.objectId = function(_id) {
    return mongoose.mongo.ObjectId.createFromHexString(_id);
}

mongodb.prototype.saveItem = function(collection, data, callback) {
    models[collection](data).save(function(err, item) {
        if (err) logger.error("Error saving", err);
        if (callback) return callback(err, [item]);
    });
};

mongodb.prototype.save = function(collection, data, callback) {
    var me = this;
    models[collection](data).save(function(err, item) {
        if (data._id) {
            return me.updateCollection(collection, { _id: data._id }, data, callback);
        }
        if (err && err.name == "ValidationError") {
            var query = {
                date_deleted: null
            };
            for (var key in err.errors) {
                query[key] = err.errors[key].value;
            }
            me.updateCollection(collection, query, data, callback);
        } else if (err && err.code === 11000) {
            var query = {
                _id: data._id,
                date_deleted: null
            };
            me.updateCollection(collection, query, data, callback);
        } else {
            if (err) {
                logger.error("Error saving", err);
            }
            if (callback) {
                return callback(err, [item]);
            }
        }
    });
};

mongodb.prototype.updateCollection = function updateCollection(collection, query, data, callback) {
    models[collection].updateMany(query, data, function(err, response) {
        models[collection].find(query, callback);
    });
}

mongodb.prototype.updateCollectionSync = function updateCollection(collection, query, data) {
    return new Promise(function(resolve) {
        models[collection].updateMany(query, data, function(err, response) {
            models[collection].find(query, function(error, item) {
                resolve({
                    error: error,
                    item: item
                })
            });
        });
    });
}

mongodb.prototype.searchWithaggregate = function(collection, paramsAggre, callback) {
    models[collection].aggregate(paramsAggre).exec(function(err, aggregateData) {
        if (err) {
            return callback(err, []);
        }
        return callback(null, aggregateData);
    });
}

mongodb.prototype.remove = function(collection, params, callback) {
    models[collection].deleteMany(params, function(err, doc) {
        if (err) {
            return callback(err, []);
        }
        return callback(null, doc);
    })
}

mongodb.prototype.saveAsync = function(collection, data) {
    return new Promise((resolve) => {
        models[collection](data).save(function(err, item) {
            if (err) logger.error("Error saving", err);
            return resolve(err ? [] : [item]);
        });
    });
};

module.exports = new mongodb();