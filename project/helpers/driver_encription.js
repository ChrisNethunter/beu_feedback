const crypto = require('crypto');
const CURRENT_ENV = process.env.NODE_ENV || 'development';
const config = require('../config.json');

class Encription {
    constructor(){
        this.secretKey = config[CURRENT_ENV].ENCRYPTION_KEY;
        this.algorithm = 'aes-256-cbc';
    }

    encrypt(text) {
        const iv = crypto.randomBytes(16);
        let cipher = crypto.createCipheriv(this.algorithm, Buffer.from(this.secretKey), iv);
        let encrypted = cipher.update(String(text));
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return { iv: iv.toString('hex'), content: encrypted.toString('hex') };
    }
   
    decrypt(text) {
        let iv = Buffer.from(text.iv, 'hex');
        let encryptedText = Buffer.from(text.content, 'hex');
        let decipher = crypto.createDecipheriv(this.algorithm, Buffer.from(this.secretKey), iv);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);
        return decrypted.toString();
    }

}
module.exports = new Encription()