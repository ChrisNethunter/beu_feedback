const mongodb = require('./mongodb.js');
const data_example = require('./data.json');
const { generatePassword } = require('./generatePassword.js');

/**
 * @class Migrations
 * @memberof Helpers
 *
 * @classdesc This class contains is used for migration control, that is, insertion of initial data for the application or correction of the same
 *
 * @author Christian Jimenez  <cristian@vifuy.com>
 *
 * @requires data_example
 * @requires generatePassword
 */

class Migrations {
    constructor() {
        this.fix_user = "fix_user"
        this.fix_categories = "fix_categories"
        this.fix_suggestions = "fix_suggestions",
            this.userPropertyRegister;
    }

    triggerFixtures() {
        this.categories();
        this.user();
    }

    async user() {
        let validate_fix = await this.validateFixture(this.fix_user);
        if (!validate_fix) {
            return false
        }

        let userData = data_example.currentUser;
        userData.email = `${userData.username}@gmail.com`;
        userData.password = await generatePassword('12345');
        mongodb.save('users', userData, (error, responseUser) => {
            if (!error) {
                this.suggestions(responseUser[0]._id)
            }
        });

        this.insertFixture(this.fix_user);
    }

    async categories() {
        let validate_fix = await this.validateFixture(this.fix_categories);
        if (!validate_fix) {
            return false
        }
        let categories = ["Feature", "UI", "UX", "Enhancement", "Bug"]
        for (let category of categories) {
            let data = {
                name: category
            }
            mongodb.save('categories', data);
        }
        this.insertFixture(this.fix_categories);
    }

    async suggestions(user) {
        let validate_fix = await this.validateFixture(this.fix_suggestions);
        if (!validate_fix) {
            return false
        }

        let data_suggestions = data_example.productRequests;
        for (let suggestion of data_suggestions) {
            let name_category = this.capitalizeFirstLetter(suggestion.category);
            let data_category = await this.findDataFix("categories", { name: name_category });

            if (data_category) {
                if (data_category.success) {
                    suggestion.category = data_category.data._id;
                    suggestion.user = user;
                    delete suggestion.comments
                    mongodb.save('suggestions', suggestion);
                }
            }
        }

        this.insertFixture(this.fix_suggestions);
    }

    validateFixture(fixture) {
        return new Promise((resolve, reject) => {
            mongodb.find('migrations', { name: fixture }, async(err, response) => {
                if (Object.keys(response).length == 0) {
                    return resolve(true)
                } else {
                    return resolve(false)
                }
            });
        });
    }

    insertFixture(fixture) {
        mongodb.save('migrations', { name: fixture });
        console.log(`execute fix ${fixture} ...`)
    }

    findDataFix(collection, filter) {
        return new Promise((resolve, reject) => {
            mongodb.find(collection, filter, async(err, response) => {
                if (Object.keys(response).length > 0) {
                    return resolve({
                        success: true,
                        data: response
                    })
                } else {
                    return resolve({
                        success: false,
                        data: {}
                    })
                }
            });
        });
    }

    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

module.exports = new Migrations();